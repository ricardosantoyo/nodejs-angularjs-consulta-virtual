var connection = require('../config/connection'); // Se manda llamar a la coneccion con MySQL.

exports.signin = function(request, response){
    //console.log("entre");
    //console.log(request.body);
    connection.acquire(function(err, con) {
        con.query('select * from user where username = ? and pass = md5( ? ) and validated = 1', [request.body.username, request.body.pass], function(err, result){
            con.release();
            if (err){
                response.status(400);
                return response.json({err: err});
            } else{
                if (result.length == 0){
                    response.status(403);
                    return response.json({err: 'Usuario o clave incorrectos, verifique sus datos.',data: result});
                } else{
                  con.query("update user set online = 1 where username = ?", request.body.username, function(err, result2){
                    if (err){
                      response.status(400);
                      return response.json({err: err});
                    } else{
                      response.status(200); // Arroja un estado 200: "La peticion resulto exitosa y se manda una respuesta a cambio"
                      return response.json({message: 'Sesion iniciada',data: result});
                    }
                  });
                }
            }
        });
    });
}

exports.signout = function(request, response){
    //console.log("entre");
    console.log(request.body);
    connection.acquire(function(err, con) {
      con.query("update user set online = 0 where username = ?", request.body.username, function(err, result){
            con.release();
            if (err){
                response.status(400);
                return response.json({err: err});
            } else{
                if (result.length == 0){
                    response.status(400);
                    return response.json({err: err, data: result});
                } else{
                    response.status(200); // Arroja un estado 200: "La peticion resulto exitosa y se manda una respuesta a cambio"
                    return response.json({message: 'Sesion finalizada',data: result});
                }
            }
        });
    });
}
