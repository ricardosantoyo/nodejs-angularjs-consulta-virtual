angular.module('consultingApp')
    .controller('DocController', function($scope, DocService, $location){ // Controlador de Usuarios manejando y requiriendo los servicios de scope, (MainService) userService.js y http
        /*if (!localStorage.getItem('username'))
          location.href='#/';*/
        $scope.newDoc = {}; // Variable para agregar un usuario al darlo de alta
        $scope.cntrlMessage = null; // Mensaje de ejecucion de alguna funcion
        $scope.isDisabled = false; // Variable para deshabilitar el boton de eliminar y actualizar para cuando no hay usuarios registrados y se desea eliminar alguno o cambiar su informacion.
        $scope.havePatiens=false;
        $scope.patientInfo = {};
        $scope.newConsult = {};
        $scope.newPat = {};

        $scope.getConsultHistory = function(){ // se crea una funcion en el controlador
            DocService.getConsultHistory($scope.searchPat) // Parametro username.
            .success(function(getData){ // Se requiere del servicio "getUSer()" desde "UserService", en caso de que el servicio resulto sin errores:
                //console.log(angular.toJson(getData.data));
                $scope.listInfoConsult = getData.data; // Se obtienen los datos y se mandan a la variable listUsers
                if ($scope.listInfoConsult == 0){ // Si no encontro datos
                    //console.log("undefined 1");
                    $scope.cntrlMessage = "No Hay Historial";
                    // Deshabilita los botones de eliminar y actualizar.
                    $scope.havePatiens=false;
                } else{
                    $scope.havePatiens=true;
                    $scope.cntrlMessage ="";
                    $scope.searchConsultHistory = $scope.listInfoConsult[0]; // Si hay registros, Coloca el primero en el combobox para delUSer y para updUser
                } //console.log($scope.delUser);
            })
            .error(function(data) { // En caso contrario, se imprime el error en consola.
                console.log('Error:');
                console.log(data);
            });

        };

        $scope.getMedicalHistory = function(){ // se crea una funcion en el controlador
            DocService.getMedicalHistory($scope.searchPat) // Parametro username.
            .success(function(getData){ // Se requiere del servicio "getUSer()" desde "UserService", en caso de que el servicio resulto sin errores:
                //console.log(angular.toJson(getData.data));
                $scope.listInfo = getData.data; // Se obtienen los datos y se mandan a la variable listUsers
                if ($scope.listInfo == 0){ // Si no encontro datos
                    //console.log("undefined 1");
                    $scope.cntrlMessage = "No Hay Expediente";
                    // Deshabilita los botones de eliminar y actualizar.
                    $scope.havePatiens=false;
                } else{
                    $scope.havePatiens=true;
                    $scope.cntrlMessage ="";
                    $scope.searchMedicalHistory = $scope.listInfo[0]; // Si hay registros, Coloca el primero en el combobox para delUSer y para updUser
                } //console.log($scope.delUser);
            })
            .error(function(data) { // En caso contrario, se imprime el error en consola.
                console.log('Error:');
                console.log(data);
            });

        };

        $scope.getPatient = function(){ // se crea una funcion en el controlador
            DocService.getPatient($scope.searchPat)
            .success(function(getData){ // Se requiere del servicio "getUSer()" desde "UserService", en caso de que el servicio resulto sin errores:
                //console.log(angular.toJson(getData.data));
                $scope.listUser = getData.data; // Se obtienen los datos y se mandan a la variable listUsers
                if ($scope.listUser == 0){ // Si no encontro datos
                    //console.log("undefined 1");
                    //$scope.isDisabled = true; // Deshabilita los botones de eliminar y actualizar.
                } else{
                    //$scope.searchPat = $scope.listUser[0]; // Si hay registros, Coloca el primero en el combobox para delUSer y para updUser
                } //console.log($scope.delUser);
            })
            .error(function(data) { // En caso contrario, se imprime el error en consola.
                console.log('Error:');
                console.log(data);
            });

        };

        $scope.getPatients = function(){ // se crea una funcion en el controlador
            DocService.getPatients()
            .success(function(getData){ // Se requiere del servicio "getUSer()" desde "UserService", en caso de que el servicio resulto sin errores:
                //console.log(angular.toJson(getData.data));
                $scope.listUsers = getData.data; // Se obtienen los datos y se mandan a la variable listUsers
                if ($scope.listUsers == 0){ // Si no encontro datos
                    //console.log("undefined 1");
                    $scope.cntrlMessage = "No hay usuarios registrados";
                    $scope.isDisabled = true; // Deshabilita los botones de eliminar y actualizar.
                } else{
                    $scope.searchPat = $scope.listUsers[0]; // Si hay registros, Coloca el primero en el combobox para delUSer y para updUser
                } //console.log($scope.delUser);
            })
            .error(function(data) { // En caso contrario, se imprime el error en consola.
                console.log('Error:');
                console.log(data);
            });

        };


        // Cuando se añade un nuevo USER, manda el texto a la API
        $scope.signup = function (){
          //console.log($scope.newUser.username);
            if ($scope.newDoc.username=="" || $scope.newDoc.username==null ||
                    $scope.newDoc.pass1=="" || $scope.newDoc.pass1==null ||
                    $scope.newDoc.pass2=="" || $scope.newDoc.pass2==null ||
                    $scope.newDoc.name=="" || $scope.newDoc.name==null ||
                    $scope.newDoc.lastnameFather=="" || $scope.newDoc.lastnameFather==null ||
                    $scope.newDoc.lastnameMother=="" || $scope.newDoc.lastnameMother==null ||
                    $scope.newDoc.email=="" || $scope.newDoc.email==null ||
                    $scope.newDoc.sex=="" || $scope.newDoc.sex==null){
                //console.log("Campo vacio");
                $scope.cntrlMessage = "Campo(s) vacío(s)";
            } else if ($scope.newDoc.pass1 != $scope.newDoc.pass2){
                $scope.cntrlMessage = "Las contraseñas no concuerdan";
            } else{
                DocService.signup($scope.newDoc)
                .success(function (getData){
                    //console.log(getData);
                    $scope.cntrlMessage = getData.message;
                    $scope.newDoc = null;
                })
                .error(function (data) {
                    //console.log(data);
                    $scope.cntrlMessage = data.err; // El back arroja un error cuando el usuario ya se encuentra registrado.
                });
            }
        };

        $scope.initVid = function(){
          var video_out = document.getElementById("vid-box");
          $scope.phone = window.phone = PHONE({
              number        : localStorage.getItem('username') || "Anonymous", // listen on username line else Anonymous
              publish_key   : 'pub-c-741f5214-a918-4243-803f-24400f84de6b', // Your Pub Key
              subscribe_key : 'sub-c-61a8bc18-39cd-11e6-b47e-0619f8945a4f', // Your Sub Key
              ssl           : (('https:' == document.location.protocol) ? true : false)
          });
          $scope.message = document.getElementById('message');
          $scope.phone.ready(function(){
            $scope.message = "Conectado satisfactoriamente";
          });
          $scope.phone.receive(function(session){
            console.log("Recibió llamada");
              session.connected(function(session) {
                PUBNUB.$("vid-box").appendChild(session.video);
                console.log("Llamada conectada");
                $scope.dateStart = $scope.fecha();
              });
              session.ended(function(session) {
                video_out.innerHTML='';
              });
          });
        };

        $scope.makeCall = function(){
          if (!window.phone) alert("Login First!");
          else $scope.phone.dial(localStorage.getItem('doctor'));
          return false;
        };

        $scope.endCall = function(){
          $scope.phone.hangup();
          $scope.phone.disconnect();
          $scope.dateEnd = $scope.fecha();
        }

        $scope.getSigns = function(){ // se crea una funcion en el controlador
            DocService.getSigns(localStorage.getItem('username'))
            .success(function(getData){ // Se requiere del servicio "getUSer()" desde "UserService", en caso de que el servicio resulto sin errores:
                //console.log(angular.toJson(getData.data));
                $scope.listUsers = getData.data; // Se obtienen los datos y se mandan a la variable listUsers
                if ($scope.listUsers == 0){ // Si no encontro datos
                    //console.log("undefined 1");
                    $scope.cntrlMessage = "No hay usuarios registrados";
                    $scope.isDisabled = true; // Deshabilita los botones de eliminar y actualizar.
                } else{
                    $scope.patientInfo.name = getData.data[0].username;
                    $scope.patientInfo.weight = getData.data[0].weight;
                    $scope.patientInfo.size = getData.data[0].size;
                    $scope.patientInfo.presion = getData.data[0].presion;
                    $scope.patientInfo.pulse = getData.data[0].pulse;
                    localStorage.setItem('patientUsername',$scope.patientInfo.name);
                } //console.log($scope.delUser);
            })
            .error(function(data) { // En caso contrario, se imprime el error en consola.
                console.log('Error:');
                console.log(data);
            });
        };

        $scope.saveConsult = function (){
            if ($scope.newConsult.diagnostic=="" || $scope.newConsult.diagnostic==null ||
                    $scope.newConsult.reseta=="" || $scope.newConsult.reseta==null){
                //console.log("Campo vacio");
                $scope.cntrlMessage = "Campo(s) vacío(s)";
            } else{
                $scope.newConsult.dateStart = $scope.dateStart;
                $scope.newConsult.dateEnd = $scope.fecha();
                DocService.saveConsult(localStorage.getItem('username'),$scope.newConsult)
                .success(function (getData){
                    //console.log(getData);
                    $scope.cntrlMessage = getData.message;
                    $location.path('/doctor/addExpedient');
                })
                .error(function (data) {
                    //console.log(data);
                    $scope.cntrlMessage = data.err; // El back arroja un error cuando el usuario ya se encuentra registrado.
                });
            }
        };

        $scope.addExpedient = function (){
          //console.log($scope.newUser.username);
            if ($scope.newPat.weight=="" || $scope.newPat.weight==null ||
                    $scope.newPat.age=="" || $scope.newPat.age==null ||
                    $scope.newPat.temperature=="" || $scope.newPat.temperature==null ||
                    $scope.newPat.presion=="" || $scope.newPat.presion==null ||
                    $scope.newPat.pulse=="" || $scope.newPat.pulse==null ||
                    $scope.newPat.malaise=="" || $scope.newPat.malaise==null ||
                    $scope.newPat.sex=="" || $scope.newPat.sex==null ||
                    $scope.newPat.bloodType=="" || $scope.newPat.bloodType==null){
                //console.log("Campo vacio");
                $scope.cntrlMessage = "Campo(s) vacío(s)";
            } else {
                DocService.addExpedient($scope.newPat, localStorage.getItem('username'))
                .success(function (getData){
                    //console.log(getData);
                    $scope.cntrlMessage = getData.message;
                    $location.path('/home');
                })
                .error(function (data) {
                    //console.log(data);
                    $scope.cntrlMessage = data.err; // El back arroja un error cuando el usuario ya se encuentra registrado.
                });
          }
        };

        $scope.fecha = function (){
          var today = new Date();
          var dd = today.getDate();
          var mm = today.getMonth()+1; //January is 0!
          var hh = today.getHours();
          var min = today.getMinutes();
          var ss = today.getSeconds();

          var yyyy = today.getFullYear();
          if(dd<10){
              dd='0'+dd
          }
          if(mm<10){
              mm='0'+mm
          }
          var dateToday = yyyy+'-'+mm+'-'+dd+" "+hh+":"+min+":"+ss;

          return dateToday;
        };

        $scope.dateStart = $scope.fecha();
        $scope.newPat.date = $scope.fecha();
        $scope.newPat.name = localStorage.getItem('patientUsername');

});
