'use strict';
/*
    Modulos de dependencia
*/
var express = require('express');
var router = express.Router();
var authentications = require('../controllers/auth.controller');

router.route('/auth/signin').post(authentications.signin);
router.route('/auth/signout').post(authentications.signout);

module.exports = router;
