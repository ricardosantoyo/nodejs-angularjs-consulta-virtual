var connection = require('../config/connection'); // Se manda llamar a la coneccion con MySQL.

exports.addPatient = function(request, response){
    connection.acquire(function(err, con){ // Se adquieren los datos para la coneccion.
        //console.log(request.body.name);
      con.query('select * from user where username = ?', request.body.username, function(err, result, fields){
        con.release(); // Se realiza la coneccion y ejecucion de querys.
        if (err){
            response.status(400); // Arroja un estado 400: "Error 400, Error general"
            return response.json({err: err});
        } else{
          if (result.length>0){
            response.status(400);
            return response.json({err: 'El nombre de usuario ya existe ',data: result});
          } else{
            //console.log(request.body);
            con.query("insert into user (username, pass, name, lastnameFather, lastnameMother, email, validated, validateToken, sex, type, online) values (?, md5( ? ), ?, ?, ?, ?, 0, '', ?, 'patient', 0)", [request.body.username, request.body.pass1, request.body.name, request.body.lastnameFather, request.body.lastnameMother, request.body.email, request.body.sex], function(err, result){
              if (err){
                response.status(400);
                return response.json({err: err});
              } else{
                response.status(200); // Arroja un estado 200: "La peticion resulto exitosa y se manda una respuesta a cambio"
                return response.json({message: 'El paciente fue creado exitosamente'});
              }
            });
          }
        }
      });
    });
}
