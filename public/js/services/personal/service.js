'use strict';

angular.module('consultingApp')
    .factory('PersonalService', function ($http){ // Servicios del usuario
        return {
            savePat: function (data){
                return $http({ // Request al Back para registrar a un nuevo usuario a la BD.
                    method  : 'POST',
                    url     : '/api/personal/addPatient',
                    headers : {'Content-Type' : 'application/x-www-form-urlencoded'},
                    data    : $.param(data)
                });
            }
        }
    });
