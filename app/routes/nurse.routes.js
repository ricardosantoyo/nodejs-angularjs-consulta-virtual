'use strict';
/*
    Modulos de dependencia
*/
var express = require('express');
var router = express.Router();
var nurses = require('../controllers/nurse.controller');

router.route('/nurse/addPatient').post(nurses.addPatient); // Se definen las rutas en el back.
router.route('/nurse/addExpedient/:username').post(nurses.addExpedient);
router.route('/nurse/addConsult').post(nurses.addConsult);
router.route('/nurse/searchExpedientHistory/:id').get(nurses.getExpedientHistory);
router.route('/nurse/searchConsultHistory/:id').get(nurses.getConsultHistory);
router.route('/nurse/searchPatients/').get(nurses.getPatients);

module.exports = router;
