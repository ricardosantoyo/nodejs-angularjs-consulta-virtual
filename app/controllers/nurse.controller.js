var connection = require('../config/connection'); // Se manda llamar a la coneccion con MySQL.

exports.addPatient = function(request, response){
    connection.acquire(function(err, con){ // Se adquieren los datos para la coneccion.
        //console.log(request.body.name);
      con.query('select * from user where username = ?', request.body.username, function(err, result, fields){
        con.release(); // Se realiza la coneccion y ejecucion de querys.
        if (err){
            response.status(400); // Arroja un estado 400: "Error 400, Error general"
            return response.json({err: err});
        } else{
          if (result.length>0){
            response.status(400);
            return response.json({err: 'El nombre de usuario ya existe ',data: result});
          } else{
            //console.log(request.body);
            con.query("insert into user (username, pass, name, lastnameFather, lastnameMother, email, validated, validateToken, sex, type, online) values (?, md5( ? ), ?, ?, ?, ?, 1, '', ?, 'patient', 0)", [request.body.username, request.body.pass1, request.body.name, request.body.lastnameFather, request.body.lastnameMother, request.body.email, request.body.sex], function(err, result){
              if (err){
                response.status(400);
                return response.json({err: err});
              } else{
                response.status(200); // Arroja un estado 200: "La peticion resulto exitosa y se manda una respuesta a cambio"
                return response.json({message: 'El usuario fue creado exitosamente'});
              }
            });
          }
        }
      });
    });
}

exports.addExpedient = function(request, response){
    var username = request.params.username;
    connection.acquire(function(err, con){ // Se adquieren los datos para la coneccion.
        //console.log(request.body.name);
      con.query('select * from user where username = ?', request.body.patient, function(err, result, fields){
        con.release(); // Se realiza la coneccion y ejecucion de querys.
        if (err){
          //console.log("entra");
            response.status(400); // Arroja un estado 400: "Error 400, Error general"
            return response.json({err: err});
        } else{
          if (result.length<=0){
            //console.log("Entra2");
            response.status(400);
            return response.json({err: 'El nombre de usuario no existe ',data: result});
          } else{
            //console.log(request.body);
            con.query("select * from user where username = ?", request.params.username, function(err, result2, fields){
              if (err){
                //console.log("entra3");
                response.status(400);
                return response.json({err: err});
              } else {
                //console.log(result[0].id);
                //console.log(result2[0].id);
                con.query("insert into medicalhistory (idUser, date, weight, height, pacientAge, temperature, presion, pulse, malaise, sex, idNurse, bloodType) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [result[0].id, request.body.date, request.body.weight, request.body.height, request.body.age, request.body.temperature, request.body.presion, request.body.pulse, request.body.malaise, request.body.sex, result2[0].id, request.body.bloodType], function(err, result3){
                  if (err){
                    response.status(400);
                    return response.json({err: err});
                  } else{
                    response.status(200); // Arroja un estado 200: "La peticion resulto exitosa y se manda una respuesta a cambio"
                    return response.json({message: 'El expediente fue creado exitosamente'});
                  }
                });
              }
            });
          }
        }
      });
    });
}

exports.addConsult = function(request, response){
    connection.acquire(function(err, con){ // Se adquieren los datos para la coneccion.
        //console.log(request.body.name);
      con.query('select * from user where username = ?', request.body.searchPat.username, function(err, result, fields){
        con.release(); // Se realiza la coneccion y ejecucion de querys.
        if (err){
          console.log("entra");
            response.status(400); // Arroja un estado 400: "Error 400, Error general"
            return response.json({err: err});
        } else{
          if (result.length<=0){
            console.log("Entra2");
            response.status(400);
            return response.json({err: 'El nombre de usuario no existe ',data: result});
          } else{
            console.log(request.body);
            con.query("insert into consult (idPatient, dateConsult, weight, size, presion, pulse) values (?, ?, ?, ?, ?, ?)", [result[0].id, request.body.date, request.body.weight, request.body.size, request.body.presion, request.body.pulse], function(err, result3){
              if (err){
                response.status(400);
                return response.json({err: err});
              } else{
                response.status(200); // Arroja un estado 200: "La peticion resulto exitosa y se manda una respuesta a cambio"
                //return response.json({message: 'La consulta fue creada exitosamente'});
                con.query("select * from user where type='doctor'", function(err, result4){
                  if (err){
                    response.status(400);
                    return response.json({err: err});
                  } else{
                    response.status(200); // Arroja un estado 200: "La peticion resulto exitosa y se manda una respuesta a cambio"
                    //return response.json({message: 'La consulta fue creada exitosamente'});
                    var len = result4.length;
                    var ran = Math.round(Math.random() * len);
                    con.query("insert into doctor_patient(idUser, idDoctor) values (?, ?)", [result[0].id, result4[ran].id],function(err, result5){
                      if (err){
                        response.status(400);
                        return response.json({err: err});
                      } else{
                        response.status(200); // Arroja un estado 200: "La peticion resulto exitosa y se manda una respuesta a cambio"
                        return response.json({message: 'La consulta fue creada exitosamente, se asignó un doctor al paciente'});
                      }
                    });
                  }
                });
              }
            });
          }
        }
      });
    });
}

exports.getExpedientHistory = function(request, response){
  //console.log(request.body);
    connection.acquire(function(err, con) {
      con.query('select mh.*,u.name,u.lastNameFather,u.lastNameMother,DATE_FORMAT(date,"%b %d %Y %h:%i %p") as fecha, CASE When mh.sex=1 THEN "F" Else "M" end as sex2 from medicalhistory mh, user u where mh.idUser = ? and u.id = mh.idNurse order by mh.date desc, mh.id asc', request.params.id, function(err, result){
        con.release();
        if (err){
          response.status(400);
          return response.json({err: err});
        } else{
          response.status(200);
          return response.json({message: 'Lista de Usuarios: ',data: result});
        }
      });
    });
}

exports.getConsultHistory = function(request, response){
  //console.log(request.body);
    connection.acquire(function(err, con) {
      con.query('select ah.*,u.name,u.lastNameFather,u.lastNameMother,DATE_FORMAT(firstDate,"%b %d %Y %h:%i %p") as fecha1,DATE_FORMAT(lastDate,"%b %d %Y %h:%i %p") as fecha2 from actualhistory ah, user u where ah.idUser = ? and u.id = ah.idDoctor order by ah.firstDate desc', request.params.id, function(err, result){
        con.release();
        if (err){
          response.status(400);
          return response.json({err: err});
        } else{
          response.status(200);
          return response.json({message: 'Lista de Usuarios: ',data: result});
        }
      });
    });
}

exports.getPatients = function(request, response){
  //console.log(request.body);
    connection.acquire(function(err, con) {
      con.query('select * from user where type="patient"', function(err, result){
        con.release();
        if (err){
          response.status(400);
          return response.json({err: err});
        } else{
          response.status(200);
          return response.json({message: 'Lista de Usuarios: ',data: result});
        }
      });
    });
}
