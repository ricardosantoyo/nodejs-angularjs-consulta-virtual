var connection = require('../config/connection'); // Se manda llamar a la coneccion con MySQL.4
var nodemailer = require('nodemailer');
var md5 = require('md5');

exports.addReceipt = function(request, response){
    connection.acquire(function(err, con){ // Se adquieren los datos para la coneccion.
        //console.log(request.body.name);
      con.query('select c.* ,u2.*,u2.id as idPacient,u.id as idDoctor from consult c, solicitud_videollamada s, user u, user u2 where u.username=? and s.idDoctor=u.id and c.idPatient=s.idPatient and u2.id=s.idPatient order by c.dateConsult desc limit 1', request.params.username,function(err, result,fields){
        con.release(); // Se realiza la coneccion y ejecucion de querys.
        if (err){
            response.status(400); // Arroja un estado 400: "Error 400, Error general"
            return response.json({err: err});
        } else{
          if (result.length>0){
            console.log(result);
            con.query("insert into actualhistory (idDoctor, idUser, firstDate, lastDate, diagnostic, reseta) values (?,?, ?, ?, ?, ?)", [result[0].idDoctor, result[0].idPacient, request.body.dateStart, request.body.dateEnd, request.body.diagnostic, request.body.reseta], function(err, result2){
              if (err){
                response.status(400);
                return response.json({err: err});
              } else{
                response.status(200); // Arroja un estado 200: "La peticion resulto exitosa y se manda una respuesta a cambio"
                return response.json({message: 'El Receta fue creada exitosamente'});
              }
            });
          } else{
            //console.log(request.body);

          }
        }
      });
    });
}
exports.addExpedient = function(request, response){
    var username = request.params.username;
    connection.acquire(function(err, con){ // Se adquieren los datos para la coneccion.
        //console.log(request.body.name);
      con.query('select * from user where username = ?', request.body.name, function(err, result, fields){
        con.release(); // Se realiza la coneccion y ejecucion de querys.
        if (err){
          //console.log("entra");
            response.status(400); // Arroja un estado 400: "Error 400, Error general"
            return response.json({err: err});
        } else{
          if (result.length<=0){
            //console.log("Entra2");
            response.status(400);
            return response.json({err: 'El nombre de usuario no existe ',data: result});
          } else{
            //console.log(request.body);
            con.query("select * from user where username = ?", request.params.username, function(err, result2, fields){
              if (err){
                //console.log("entra3");
                response.status(400);
                return response.json({err: err});
              } else {
                //console.log(result[0].id);
                //console.log(result2[0].id);
                con.query("insert into medicalhistory (idUser, date, weight, height, pacientAge, temperature, presion, pulse, malaise, sex, idNurse, bloodType) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [result[0].id, request.body.date, request.body.weight, request.body.height, request.body.age, request.body.temperature, request.body.presion, request.body.pulse, request.body.malaise, request.body.sex, result2[0].id, request.body.bloodType], function(err, result3){
                  if (err){
                    response.status(400);
                    return response.json({err: err});
                  } else{
                    response.status(200); // Arroja un estado 200: "La peticion resulto exitosa y se manda una respuesta a cambio"
                    return response.json({message: 'El expediente fue creado exitosamente'});
                  }
                });
              }
            });
          }
        }
      });
    });
}
exports.getMedicalHistory = function(request, response){
  //console.log(request.body);
    connection.acquire(function(err, con) {
      con.query('select mh.*,u.name,u.lastNameFather,u.lastNameMother,DATE_FORMAT(date,"%b %d %Y %h:%i %p") as fecha, CASE When mh.sex=1 THEN "F" Else "M" end as sex2 from medicalhistory mh, user u where mh.idUser = ? and u.id = mh.idNurse order by mh.date desc, mh.id asc', request.params.id, function(err, result){
        con.release();
        if (err){
          response.status(400);
          return response.json({err: err});
        } else{
          response.status(200);
          return response.json({message: 'Lista de Usuarios: ',data: result});
        }
      });
    });
}

exports.getConsultHistory = function(request, response){
  //console.log(request.body);
    connection.acquire(function(err, con) {
      con.query('select ah.*,u.name,u.lastNameFather,u.lastNameMother,DATE_FORMAT(firstDate,"%b %d %Y %h:%i %p") as fecha1,DATE_FORMAT(lastDate,"%b %d %Y %h:%i %p") as fecha2 from actualhistory ah, user u where ah.idUser = ? and u.id = ah.idDoctor order by ah.firstDate desc', request.params.id, function(err, result){
        con.release();
        if (err){
          response.status(400);
          return response.json({err: err});
        } else{
          response.status(200);
          return response.json({message: 'Lista de Usuarios: ',data: result});
        }
      });
    });
}

exports.getPatient = function(request, response){
  //console.log(request.body);
    connection.acquire(function(err, con) {
      con.query('select * ,CASE When sex=1 THEN "F" Else "M" end as sex2  from `user` where id=?', request.params.id,function(err, result){
        con.release();
        if (err){
          response.status(400);
          return response.json({err: err});
        } else{
          response.status(200);
          return response.json({message: 'Lista de Usuarios: ',data: result});
        }
      });
    });
}

exports.getPatients = function(request, response){
  //console.log(request.body);
    connection.acquire(function(err, con) {
      con.query('select * from user where type="patient"', function(err, result){
        con.release();
        if (err){
          response.status(400);
          return response.json({err: err});
        } else{
          response.status(200);
          return response.json({message: 'Lista de Usuarios: ',data: result});
        }
      });
    });
}

exports.signup = function(request, response){
    connection.acquire(function(err, con){ // Se adquieren los datos para la coneccion.
        //console.log(request.body.name);
      con.query('select * from user where username = ?', request.body.username, function(err, result, fields){
        con.release(); // Se realiza la coneccion y ejecucion de querys.
        if (err){
            response.status(400); // Arroja un estado 400: "Error 400, Error general"
            return response.json({err: err});
        } else{
          if (result.length>0){
            response.status(400);
            return response.json({err: 'El nombre de usuario ya existe ',data: result});
          } else{
            //console.log(request.body);
            con.query("insert into user (username, pass, name, lastnameFather, lastnameMother, email, validated, validateToken, sex, type, online) values (?, md5( ? ), ?, ?, ?, ?, 0, md5( ? ), ?, 'doctor', 0)", [request.body.username, request.body.pass1, request.body.name, request.body.lastnameFather, request.body.lastnameMother, request.body.email, request.body.username, request.body.sex], function(err, result, fields){
              if (err){
                response.status(400);
                return response.json({err: err});
              } else{

                // Create a SMTP transport object
                var transport = nodemailer.createTransport("SMTP", {
                        service: 'Gmail',
                        auth: {
                            user: "consultavirtualproyecto@gmail.com",//Cuenta
                            pass: "consultavirtualproyecto1"//Contraseña
                        }
                    });

                console.log('SMTP Configured');

                console.log(result);
                // Message object
                var message = {

                    // sender info
                    from: 'Consulta Virtual <consultavirtualproyecto@gmail.com>',//Quien lo manda

                    // Comma separated list of recipients
                    to: '"'+request.body.name+' '+request.body.lastnameFather+'" <'+request.body.email+'>',//Quien lo manda

                    // Subject of the message
                    subject: 'Verifica tu cuenta ✔',//Título del mail

                    // plaintext body
                    text: 'Activa tu cuenta dando clic en el siguiente enlace',//Texto plano

                    // cuerpo HTML

                    html: "<p>Activa tu cuenta dando clic en el siguiente enlace: <a href='https://localhost:8000/api/doctor/verify/"+md5(request.body.username)+"'> http://localhost:3000/api/doctor/verify/"+md5(request.body.username)+" </a></p>"
                };

                console.log('Sending Mail');
                transport.sendMail(message, function(error){
                  if(error){
                      console.log('Error occured');
                      console.log(error.message);
                      return;
                  }
                  console.log('Message sent successfully!');

                  // if you don't want to use this transport object anymore, uncomment following line
                  //transport.close(); // close the connection pool
                });

                response.status(200); // Arroja un estado 200: "La peticion resulto exitosa y se manda una respuesta a cambio"
                return response.json({message: 'Se ha registrado exitosamente. Verifique su correo electrónico para darse de alta al sistema.'});
              }
            });
          }
        }
      });
    });
}
exports.getSigns = function(request, response){
  //console.log(request.body);
    connection.acquire(function(err, con) {
      con.query('select c.* ,u2.*  from consult c, solicitud_videollamada s, user u, user u2 where u.username=? and s.idDoctor=u.id and c.idPatient=s.idPatient and u2.id=s.idPatient order by c.dateConsult desc limit 1', request.params.username,function(err, result){
        con.release();
        if (err){
          response.status(400);
          return response.json({err: err});
        } else{
          response.status(200);
          return response.json({message: 'Lista de Usuarios: ',data: result});
        }
      });
    });
}
exports.verify = function(request, response){
    connection.acquire(function(err, con){ // Se adquieren los datos para la coneccion.
        //console.log(request.body.name);
      con.query('select * from user where validateToken = ?', request.params.token, function(err, result, fields){
        con.release(); // Se realiza la coneccion y ejecucion de querys.
        if (err){
            response.status(400); // Arroja un estado 400: "Error 400, Error general"
            return response.json({err: err});
        } else{
          if (result.length == 0){
            response.status(400);
            return response.json({err: err, data: result});
          } else{
            //console.log(request.body);
            con.query('update user set validated = 1 where validateToken = ?', request.params.token, function(err, result, fields){
              if (err){
                response.status(400);
                return response.json({err: err});
              } else{
                //response.status(200); // Arroja un estado 200: "La peticion resulto exitosa y se manda una respuesta a cambio"
                response.writeHead(301,{
                  Location: 'http://localhost:3000/#/doctor/welcome'
                });
                response.end();
                //window.location="/#/signin";
                //return response.json({message: 'Se ha Validado exitosamente su correo'});
              }
            });
          }
        }
      });
    });
}
