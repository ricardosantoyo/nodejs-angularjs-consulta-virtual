'use strict';

angular.module('consultingApp')
    .factory('NurseService', function ($http){ // Servicios del usuario
        return {
            savePat: function (data){
                return $http({ // Request al Back para registrar a un nuevo usuario a la BD.
                    method  : 'POST',
                    url     : '/api/nurse/addPatient',
                    headers : {'Content-Type' : 'application/x-www-form-urlencoded'},
                    data    : $.param(data)
                });
            },
            addExpedient: function (data, data2){
                return $http({ // Request al Back para registrar a un nuevo usuario a la BD.
                    method  : 'POST',
                    url     : '/api/nurse/addExpedient/'+data2,
                    headers : {'Content-Type' : 'application/x-www-form-urlencoded'},
                    data    : $.param(data)
                });
            },
            getPatientHistory: function (data){ // Request al Back para obtener a todos los usuarios de la BD.
                return $http.get('/api/nurse/searchExpedientHistory/' + data.id);
            },
            getConsultHistory: function (data){ // Request al Back para obtener a todos los usuarios de la BD.
                return $http.get('/api/nurse/searchConsultHistory/' + data.id);
            },
            getPatients: function (){ // Request al Back para obtener a todos los usuarios de la BD.
                return $http.get('/api/nurse/searchPatients/');
            },
            addConsult: function (data){
                return $http({ // Request al Back para registrar a un nuevo usuario a la BD.
                    method  : 'POST',
                    url     : '/api/nurse/addConsult/',
                    headers : {'Content-Type' : 'application/x-www-form-urlencoded'},
                    data    : $.param(data)
                });
            }
        }
    });
