'use strict';

angular.module('consultingApp')
    .factory('DocService', function ($http){ // Servicios del usuario
        return {
            addExpedient: function (data, data2){
                return $http({ // Request al Back para registrar a un nuevo usuario a la BD.
                    method  : 'POST',
                    url     : '/api/doctor/addExpedient/'+data2,
                    headers : {'Content-Type' : 'application/x-www-form-urlencoded'},
                    data    : $.param(data)
                });
            },
            getConsultHistory: function (data){ // Request al Back para obtener a todos los usuarios de la BD.
                return $http.get('/api/doctor/searchConsultHistory/' + data.id);
            },
            getMedicalHistory: function (data){ // Request al Back para obtener a todos los usuarios de la BD.
                return $http.get('/api/doctor/searchMedicalHistory/' + data.id);
            },
            getPatient: function (data){ // Request al Back para obtener a todos los usuarios de la BD.
                return $http.get('/api/doctor/searchPatient/' + data.id);
            },
            getPatients: function (){ // Request al Back para obtener a todos los usuarios de la BD.
                return $http.get('/api/doctor/searchPatients/');
            },
            getSigns: function (data){ // Request al Back para obtener a todos los usuarios de la BD.
                return $http.get('/api/doctor/searchSigns/' + data);
            },
            signup: function (data){
                return $http({ // Request al Back para registrar a un nuevo usuario a la BD.
                    method  : 'POST',
                    url     : '/api/doctor/signup',
                    headers : {'Content-Type' : 'application/x-www-form-urlencoded'},
                    data    : $.param(data)
                });
            },
            saveConsult: function (data, data2){
                return $http({ // Request al Back para registrar a un nuevo usuario a la BD.
                    method  : 'POST',
                    url     : '/api/doctor/addReceipt/'+data,
                    headers : {'Content-Type' : 'application/x-www-form-urlencoded'},
                    data    : $.param(data2)
                });
            },
        }
    });
