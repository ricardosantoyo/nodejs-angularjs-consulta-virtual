'use strict';

angular.module('consultingApp')
    .factory('AuthService', function ($http){ // Servicios del usuario
        return {
            signin: function (data){
                return $http({ // Request al Back para registrar a un nuevo usuario a la BD.
                    method  : 'POST',
                    url     : '/api/auth/signin',
                    headers : {'Content-Type' : 'application/x-www-form-urlencoded'},
                    data    : $.param(data)
                });
            },
            signout: function (data){
                return $http({ // Request al Back para registrar a un nuevo usuario a la BD.
                    method  : 'POST',
                    url     : '/api/auth/signout',
                    headers : {'Content-Type' : 'application/x-www-form-urlencoded'},
                    data    : $.param(data)
                });
            }
        }
    });
