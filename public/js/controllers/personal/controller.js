angular.module('consultingApp')
    .controller('PersonalController', function($scope, PersonalService){ // Controlador de Usuarios manejando y requiriendo los servicios de scope, (MainService) userService.js y http
        /*if (!localStorage.getItem('username'))
          location.href='#/';*/
        $scope.newPat = {}; // Variable para agregar un usuario al darlo de alta
        $scope.cntrlMessage = null; // Mensaje de ejecucion de alguna funcion

        // Cuando se añade un nuevo USER, manda el texto a la API
        $scope.createPat = function (){
          //console.log($scope.newUser.username);
            if ($scope.newPat.username=="" || $scope.newPat.username==null ||
                    $scope.newPat.pass1=="" || $scope.newPat.pass1==null ||
                    $scope.newPat.pass2=="" || $scope.newPat.pass2==null ||
                    $scope.newPat.name=="" || $scope.newPat.name==null ||
                    $scope.newPat.lastnameFather=="" || $scope.newPat.lastnameFather==null ||
                    $scope.newPat.lastnameMother=="" || $scope.newPat.lastnameMother==null ||
                    $scope.newPat.email=="" || $scope.newPat.email==null ||
                    $scope.newPat.sex=="" || $scope.newPat.sex==null){
                //console.log("Campo vacio");
                $scope.cntrlMessage = "Campo(s) vacío(s)";
            } else if ($scope.newPat.pass1 != $scope.newPat.pass2){
                $scope.cntrlMessage = "Las contraseñas no concuerdan";
            } else{
                PersonalService.savePat($scope.newPat)
                .success(function (getData){
                    //console.log(getData);
                    $scope.cntrlMessage = getData.message;
                    $scope.newPat = null;
                })
                .error(function (data) {
                    //console.log(data);
                    $scope.cntrlMessage = data.err; // El back arroja un error cuando el usuario ya se encuentra registrado.
                });
            }
        };
});
