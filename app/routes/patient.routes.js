'use strict';
/*
    Modulos de dependencia
*/
var express = require('express');
var router = express.Router();
var patients = require('../controllers/patient.controller');

router.route('/patient/getAssigned/:username').get(patients.getAssigned);

module.exports = router;
