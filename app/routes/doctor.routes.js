'use strict';
/*
    Modulos de dependencia
*/
var express = require('express');
var router = express.Router();
var doctors = require('../controllers/doctor.controller');

router.route('/doctor/addReceipt/:username').post(doctors.addReceipt);
router.route('/doctor/addExpedient/:username').post(doctors.addExpedient);
router.route('/doctor/searchMedicalHistory/:id').get(doctors.getMedicalHistory);
router.route('/doctor/searchConsultHistory/:id').get(doctors.getConsultHistory);
router.route('/doctor/searchPatient/:id').get(doctors.getPatient);
router.route('/doctor/searchPatients/').get(doctors.getPatients);
router.route('/doctor/signup').post(doctors.signup);
router.route('/doctor/searchSigns/:username').get(doctors.getSigns);
router.route('/doctor/verify/:token').get(doctors.verify);

module.exports = router;
