'use strict';

angular.module('consultingApp')
    .factory('UserService', function ($http){ // Servicios del usuario
        return {
            getUser: function (){ // Request al Back para obtener a todos los usuarios de la BD.
                return $http.get('/api/user/search/');
            },
            getUser: function (data){ // Request al Back para obtener a todos los usuarios de la BD.
                return $http.get('/api/user/search/' + data);
            },
            saveUser: function (data){
                return $http({ // Request al Back para registrar a un nuevo usuario a la BD.
                    method  : 'POST',
                    url     : '/api/user/add',
                    headers : {'Content-Type' : 'application/x-www-form-urlencoded'},
                    data    : $.param(data)
                });
            },
            updateUser: function (data1, data2){
                return $http({ // Request al Back para actualizar la informacion de un usuario de la BD.
                    method  : 'PUT',
                    url     : '/api/user/update/' + data1.id,
                    headers : {'Content-Type' : 'application/x-www-form-urlencoded'},
                    data    : $.param(data2)
                });
            },
            destroyUser: function (id){ // Request al Back para eliminar a un usuario de la BD.
                return $http.delete('/api/user/delete/' + id);
            }
        }
    });
