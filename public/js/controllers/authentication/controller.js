angular.module('consultingApp')
    .controller('AuthController', function($scope, AuthService, $location){
        $scope.userdata = {
            username : null,
            pass : null
        };

        $scope.signin = function(){ // se crea una funcion en el controlador
            AuthService.signin($scope.userdata)
            .success(function(getData){
                //console.log(angular.toJson(getData.data));
                $scope.searchAuth = getData.data; // Se obtienen los datos y se mandan a la variable listUsers
                if ($scope.searchAuth == 0){
                    $scope.cntrlMessage = getData.err;
                } else{
                    localStorage.setItem('username',getData.data[0].username);
                    localStorage.setItem('type',getData.data[0].type);
                    console.log(getData.data[0].type);
                    console.log(getData.data);
                    //localStorage.setItem('username',$scope.userdata.username);
                    //console.log(localStorage.getItem('username'));
                    $location.path('/home');
                } //console.log($scope.delUser);
            })
            .error(function(data) { // En caso contrario, se imprime el error en consola.
                console.log(data);
                $scope.cntrlMessage = data.err;
            });
        };

        $scope.signout = function(){ // se crea una funcion en el controlador
          $scope.userdata.username=localStorage.getItem('username');
          AuthService.signout($scope.userdata)
          .success(function(getData){
              //console.log(angular.toJson(getData.data));
              localStorage.removeItem('username');
              localStorage.removeItem('type');
              //console.log(localStorage.getItem('username'));
              $location.path('/');
          })
          .error(function(data) { // En caso contrario, se imprime el error en consola.
              console.log(data);
              $scope.cntrlMessage = data.err;
          });
        };
});
