'use strict';
/*
    Modulos de dependencia
*/
var express = require('express');
var router = express.Router();
var users = require('../controllers/users.controller');

router.route('/user/add').post(users.addUser); // Se definen las rutas en el back.
router.route('/user/delete/:id').delete(users.deleteUser);
router.route('/user/update/:id').put(users.updateUser);
router.route('/user/search').get(users.searchUsers);
router.route('/user/search/:username').get(users.findAllBut);
router.route('/user/find/:username').get(users.findUser);

module.exports = router;
