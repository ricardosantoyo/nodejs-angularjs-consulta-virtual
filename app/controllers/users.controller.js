var connection = require('../config/connection'); // Se manda llamar a la coneccion con MySQL.

exports.addUser = function(request, response){
    connection.acquire(function(err, con){ // Se adquieren los datos para la coneccion.
        //console.log(request.body.name);
      con.query('select * from user where username = ?', request.body.username, function(err, result, fields){
        con.release(); // Se realiza la coneccion y ejecucion de querys.
        if (err){
            response.status(400); // Arroja un estado 400: "Error 400, Error general"
            return response.json({err: err});
        } else{
          if (result.length>0){
            response.status(400);
            return response.json({err: 'El nombre de usuario ya existe ',data: result});
          } else{
            //console.log(request.body);
            con.query("insert into user (username, pass, name, lastnameFather, lastnameMother, email, validated, validateToken, sex, type, online) values (?, md5( ? ), ?, ?, ?, ?, 1, '', ?, ?, 0)", [request.body.username, request.body.pass1, request.body.name, request.body.lastnameFather, request.body.lastnameMother, request.body.email, request.body.sex, request.body.type], function(err, result){
              if (err){
                response.status(400);
                return response.json({err: err});
              } else{
                response.status(200); // Arroja un estado 200: "La peticion resulto exitosa y se manda una respuesta a cambio"
                return response.json({message: 'El usuario fue creado exitosamente'});
              }
            });
          }
        }
      });
    });
}

exports.deleteUser = function(request, response){
    connection.acquire(function(err, con){
      con.query('delete from user where id = ?', request.params.id, function(err, result){
        con.release();
        if (err){
          response.status(404);
          return response.json({err: err});
        } else{
          response.status(200);
          return response.json({message: 'El usuario fue eliminado exitosamente'});
        }
      });
    });
}

exports.updateUser = function(request, response){
    var cad="update user set ";
    connection.acquire(function(err, con){
        con.release();
        if (request.body.name)
            con.query('update user set name = ? where id = ?', [request.body.name, request.params.id], function(err, result){
                if (err){
                  response.status(404);
                  return response.json({err: err});
                }
        });
        if (request.body.lastnameFather)
            con.query('update user set lastnameFather = ? where id = ?', [request.body.lastnameFather, request.params.id], function(err, result){
              if (err){
                response.status(404);
                return response.json({err: err});
              }
        });
        if (request.body.lastnameMother)
            con.query('update user set lastnameMother = ? where id = ?', [request.body.lastnameMother, request.params.id], function(err, result){
              if (err){
                response.status(404);
                return response.json({err: err});
              }
        });
        if (request.body.email)
            con.query('update user set email = ? where id = ?', [request.body.email, request.params.id], function(err, result){
              if (err){
                response.status(404);
                return response.json({err: err});
              }
        });
        if (request.body.sex)
            con.query('update user set sex = ? where id = ?', [request.body.sex, request.params.id], function(err, result){
              if (err){
                response.status(404);
                return response.json({err: err});
              }
        });
        if (request.body.type)
            con.query('update user set type = ? where id = ?', [request.body.type, request.params.id], function(err, result){
              if (err){
                response.status(404);
                return response.json({err: err});
              }
        });
        response.status(200);
        return response.json({message: 'La informacion del usuario fue actualizada exitosamente', username: request.body.name});
    });
}

exports.searchUsers = function(request, response){
  //console.log(request.body);
    connection.acquire(function(err, con) {
      con.query('select * from user', function(err, result){
        con.release();
        if (err){
          response.status(400);
          return response.json({err: err});
        } else{
          response.status(200);
          return response.json({message: 'Lista de Usuarios: ',data: result});
        }
      });
    });
}

exports.findAllBut = function(request, response){
  //console.log(request.body);
    connection.acquire(function(err, con) {
      con.query('select * from user where username <> ?', request.params.username, function(err, result){
        con.release();
        if (err){
          response.status(400);
          return response.json({err: err});
        } else{
          response.status(200);
          return response.json({message: 'Lista de Usuarios: ',data: result});
        }
      });
    });
}
// PROBAR
exports.findUser = function(request, response){
  //console.log(request.body);
    connection.acquire(function(err, con) {
      con.query('select * from user where username = ?', request.params.username, function(err, result){
        con.release();
        if (err){
          response.status(400);
          return response.json({err: err});
        } else{
          response.status(200);
          return response.json({message: 'Lista de Usuarios: ',data: result});
        }
      });
    });
}
