'use strict';

angular.module('consultingApp').directive('menu', function(){ // Se crea la directiva menu.
    return {
        templateUrl : '../views/main/menu.html', // devuelve la vista del menu
        /*
        The restrict option is typically set to:

          'A' - only matches attribute name
          'E' - only matches element name
          'C' - only matches class name
          'M' - only matches comment

        */
        restrict    : 'A', // Restriccion A
        controller  : ['$scope', '$location', function($scope, $location){ // Controlador encargado de realizar las redirecciones del menu
            var url = $location.path().split('/');
            $scope.sections = [ // La que todo usuario debe tener
                {'title' : 'Home', 'route' : '/home', 'active' : $location.path() === '/' ? true : false}
            ]; // Si es administrador, mostrarle la ruta de usuarios
            if (localStorage.getItem('type') == "admin")
                $scope.sections.push({'title' : 'Usuarios', 'route' : '/users', 'active' : $location.path() === '/users' ? true : false});
            if (localStorage.getItem('type') == "personal")
                $scope.sections.push({'title' : 'Registo pacientes', 'route' : '/personal', 'active' : $location.path() === '/personal' ? true : false});
            if (localStorage.getItem('type') == "doctor"){
                $scope.sections.push({'title' : 'Historial De Consultas', 'route' : '/doctor/ConsultHistory', 'active' : $location.path() === '/doctor/ConsultHistory' ? true : false});
                $scope.sections.push({'title' : 'Expediente Clínico', 'route' : '/doctor/MedicalHistory', 'active' : $location.path() === '/doctor/MedicalHistory' ? true : false});
            } if (localStorage.getItem('type') == "nurse"){
                $scope.sections.push({'title' : 'Registro pacientes', 'route' : '/nurse/addPatient', 'active' : $location.path() === '/nurse/addPatient' ? true : false});
                $scope.sections.push({'title' : 'Registro de expedientes', 'route' : '/nurse/addExpedient', 'active' : $location.path() === '/nurse/addExpedient' ? true : false});
                $scope.sections.push({'title' : 'Consulta', 'route' : '/nurse/addConsult', 'active' : $location.path() === '/nurse/addConsult' ? true : false});
                $scope.sections.push({'title' : 'Recetas', 'route' : '/nurse/receipt', 'active' : $location.path() === '/nurse/receipt' ? true : false});
            } if (localStorage.getItem('type') == "patient")
                $scope.sections.push({'title' : 'Videollamada', 'route' : '/patient/videocall', 'active' : $location.path() === '/patient/videocall' ? true : false});

            //if (localStorage.getItem('type') == "patient")
            //    $scope.sections.push({'title' : 'Usuarios', 'route' : '/users', 'active' : $location.path() === '/users' ? true : false});
            // Damos de alta la o las ultimas rutas.
            $scope.sections.push({'title' : 'Cerrar sesion', 'route' : '/signout', 'active' : $location.path() === '/signout' ? true : false});
            angular.forEach($scope.sections, function(section){
                if(section.active == true){
                    $scope.title = section.title;
                }
            });
        }]
    };
});
