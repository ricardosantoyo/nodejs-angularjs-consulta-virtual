var express = require('express');
var bodyparser = require('body-parser');
var http = require('http');
var https = require('https');
var fs = require('fs');

var options = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem')
}

var connection = require('./app/config/connection');
// Se agregan todas las rutas al servidor.
var authRoutes = require('./app/routes/auth.routes');
var usersRoutes = require('./app/routes/users.routes');
var docRoutes = require('./app/routes/doctor.routes');
var nurseRoutes = require('./app/routes/nurse.routes');
var patientRoutes = require('./app/routes/patient.routes');
var personalRoutes = require('./app/routes/personal.routes');

var app = express();

connection.init(); // se inicia la coneccion a la BD.

app.use(bodyparser.urlencoded({extended: true})); // Facilita la lectura de los request generando el body en el.
app.use(bodyparser.json()); // Maneja JSON en el body del request.

app.use(express.static(__dirname + '/public')); // Le indicamos donde estan ubicadas las vistas y se cargan de forma estatica.

app.use('/api', authRoutes); // Le indicamos que la app usara estas rutas en el back partiendo desde localhost:3000/api.
app.use('/api', usersRoutes); // Le indicamos que la app usara estas rutas en el back partiendo desde localhost:3000/api.
app.use('/api', docRoutes);
app.use('/api', nurseRoutes);
app.use('/api', patientRoutes);
app.use('/api', personalRoutes);
/*app.listen(3000, function () { // Inicializamos el proyecto utilizando el puerto que indiquemos.
    //console.log('Listening on port 3000');
});*/

var httpServer = http.createServer(app);
var httpsServer = https.createServer(options, app);

//httpServer.listen(3000);
httpsServer.listen(8000);
