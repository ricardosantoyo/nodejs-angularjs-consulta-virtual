'use strict';
/*
    Modulos de dependencia
*/
var express = require('express');
var router = express.Router();
var personals = require('../controllers/personal.controller');

router.route('/personal/addPatient').post(personals.addPatient); // Se definen las rutas en el back.

module.exports = router;
