angular.module('consultingApp')
    .controller('UserController', function($scope, UserService){ // Controlador de Usuarios manejando y requiriendo los servicios de scope, (MainService) userService.js y http
        /*if (!localStorage.getItem('username'))
          location.href='#/';*/
        $scope.newUser = {}; // Variable para agregar un usuario al darlo de alta
        $scope.cntrlMessage = null; // Mensaje de ejecucion de alguna funcion
        $scope.isDisabled = false; // Variable para deshabilitar el boton de eliminar y actualizar para cuando no hay usuarios registrados y se desea eliminar alguno o cambiar su informacion.

        $scope.searchUser = function(){ // se crea una funcion en el controlador
            UserService.getUser(localStorage.getItem('username')) // Parametro username.
            .success(function(getData){ // Se requiere del servicio "getUSer()" desde "UserService", en caso de que el servicio resulto sin errores:
                //console.log(angular.toJson(getData.data));
                $scope.listUsers = getData.data; // Se obtienen los datos y se mandan a la variable listUsers
                if ($scope.listUsers == 0){ // Si no encontro datos
                    //console.log("undefined 1");
                    $scope.cntrlMessage = "No hay usuarios registrados";
                    $scope.isDisabled = true; // Deshabilita los botones de eliminar y actualizar.
                } else{
                    $scope.delUser = $scope.listUsers[0]; // Si hay registros, Coloca el primero en el combobox para delUSer y para updUser
                    $scope.updUser = $scope.listUsers[0];
                } //console.log($scope.delUser);
            })
            .error(function(data) { // En caso contrario, se imprime el error en consola.
                console.log('Error:');
                console.log(data);
            });

        };

        $scope.searchUsers = function(){ // se crea una funcion en el controlador
            UserService.getUser()
            .success(function(getData){ // Se requiere del servicio "getUSer()" desde "UserService", en caso de que el servicio resulto sin errores:
                //console.log(angular.toJson(getData.data));
                $scope.listUsers = getData.data; // Se obtienen los datos y se mandan a la variable listUsers
                if ($scope.listUsers == 0){ // Si no encontro datos
                    //console.log("undefined 1");
                    $scope.cntrlMessage = "No hay usuarios registrados";
                    $scope.isDisabled = true; // Deshabilita los botones de eliminar y actualizar.
                } else{
                    $scope.delUser = $scope.listUsers[0]; // Si hay registros, Coloca el primero en el combobox para delUSer y para updUser
                    $scope.updUser = $scope.listUsers[0];
                } //console.log($scope.delUser);
            })
            .error(function(data) { // En caso contrario, se imprime el error en consola.
                console.log('Error:');
                console.log(data);
            });

        };

        // Cuando se añade un nuevo USER, manda el texto a la API
        $scope.createUser = function (){
          //console.log($scope.newUser.username);
            if ($scope.newUser.username=="" || $scope.newUser.username==null ||
                    $scope.newUser.pass1=="" || $scope.newUser.pass1==null ||
                    $scope.newUser.pass2=="" || $scope.newUser.pass2==null ||
                    $scope.newUser.name=="" || $scope.newUser.name==null ||
                    $scope.newUser.lastnameFather=="" || $scope.newUser.lastnameFather==null ||
                    $scope.newUser.lastnameMother=="" || $scope.newUser.lastnameMother==null ||
                    $scope.newUser.email=="" || $scope.newUser.email==null ||
                    $scope.newUser.sex=="" || $scope.newUser.sex==null ||
                    $scope.newUser.type=="" || $scope.newUser.type==null){
                //console.log("Campo vacio");
                $scope.cntrlMessage = "Campo(s) vacío(s)";
            } else if ($scope.newUser.pass1 != $scope.newUser.pass2){
                $scope.cntrlMessage = "Las contraseñas no concuerdan";
            } else{
                UserService.saveUser($scope.newUser)
                .success(function (getData){
                    //console.log(getData);
                    $scope.cntrlMessage = getData.message;
                    $scope.newUser = null;
                })
                .error(function (data) {
                    //console.log(data);
                    $scope.cntrlMessage = data.err; // El back arroja un error cuando el usuario ya se encuentra registrado.
                });
          }
        };

        // Borra un USER despues de checkearlo como acabado
        $scope.deleteUser = function(id) {
              //console.log("entre");
              UserService.destroyUser($scope.delUser.id)
              .success(function(getData){
                  //console.log(getData);
                  $scope.cntrlMessage = getData.message;
                  $scope.searchUser(); // Actualiza la caja de datos para renovar al usuario eliminado
              })
              .error(function(data) {
                  console.log('Error:');
                  console.log(data);
              });
        };

        $scope.updateUser = function(id) {
          if (($scope.newUser.name=="" || $scope.newUser.name==null) &&
                  ($scope.newUser.lastnameFather=="" || $scope.newUser.lastnameFather==null) &&
                  ($scope.newUser.lastnameMother=="" || $scope.newUser.lastnameMother==null) &&
                  ($scope.newUser.email=="" || $scope.newUser.email==null) &&
                  ($scope.newUser.sex=="" || $scope.newUser.sex==null) &&
                  ($scope.newUser.type=="" || $scope.newUser.type==null)){
              //console.log("Campo vacio");
              $scope.cntrlMessage = "Campo(s) vacío(s)";
          } else if ($scope.newUser.pass1 != $scope.newUser.pass2){
              $scope.cntrlMessage = "Las contraseñas no concuerdan";
          } else{
              //console.log("entre");
              UserService.updateUser($scope.updUser, $scope.newUser)
              .success(function(getData){
                  //console.log(getData.username);
                  localStorage.setItem('username',getData.username);
                  //console.log(localStorage.getItem("username"));
                  $scope.cntrlMessage = getData.message;
                  //localStorage.setItem('username',$scope.userdata.username);
                  $scope.searchUsers(); // Actualiza la caja de datos para renovar al usuario actualizado
                  $scope.newUser = null;
              })
              .error(function(data) {
                  console.log('Error:' + data);
              });
          }
        };
});
