angular.module('consultingApp')
    .controller('NurseController', function($scope, NurseService, $location){ // Controlador de Usuarios manejando y requiriendo los servicios de scope, (MainService) userService.js y http
        /*if (!localStorage.getItem('username'))
          location.href='#/';*/
        $scope.newPat = {}; // Variable para agregar un usuario al darlo de alta
        $scope.cntrlMessage = null; // Mensaje de ejecucion de alguna funcion
        $scope.isDisabled = false;
        $scope.havePatiens=false;

        // Cuando se añade un nuevo USER, manda el texto a la API
        $scope.createPat = function (){
          //console.log($scope.newUser.username);
            if ($scope.newPat.username=="" || $scope.newPat.username==null ||
                    $scope.newPat.pass1=="" || $scope.newPat.pass1==null ||
                    $scope.newPat.pass2=="" || $scope.newPat.pass2==null ||
                    $scope.newPat.name=="" || $scope.newPat.name==null ||
                    $scope.newPat.lastnameFather=="" || $scope.newPat.lastnameFather==null ||
                    $scope.newPat.lastnameMother=="" || $scope.newPat.lastnameMother==null ||
                    $scope.newPat.email=="" || $scope.newPat.email==null ||
                    $scope.newPat.sex=="" || $scope.newPat.sex==null){
                //console.log("Campo vacio");
                $scope.cntrlMessage = "Campo(s) vacío(s)";
            } else if ($scope.newPat.pass1 != $scope.newPat.pass2){
                $scope.cntrlMessage = "Las contraseñas no concuerdan";
            } else {
                NurseService.savePat($scope.newPat)
                .success(function (getData){
                    //console.log(getData);
                    $scope.cntrlMessage = getData.message;
                    $location.path('/nurse/addExpedient');
                })
                .error(function (data) {
                    //console.log(data);
                    $scope.cntrlMessage = data.err; // El back arroja un error cuando el usuario ya se encuentra registrado.
                });
          }
        };

        $scope.addExpedient = function (){
          //console.log($scope.newUser.username);
            if ($scope.newPat.patient=="" || $scope.newPat.patient==null ||
                    $scope.newPat.weight=="" || $scope.newPat.weight==null ||
                    $scope.newPat.age=="" || $scope.newPat.age==null ||
                    $scope.newPat.temperature=="" || $scope.newPat.temperature==null ||
                    $scope.newPat.presion=="" || $scope.newPat.presion==null ||
                    $scope.newPat.pulse=="" || $scope.newPat.pulse==null ||
                    $scope.newPat.malaise=="" || $scope.newPat.malaise==null ||
                    $scope.newPat.sex=="" || $scope.newPat.sex==null ||
                    $scope.newPat.bloodType=="" || $scope.newPat.bloodType==null){
                //console.log("Campo vacio");
                $scope.cntrlMessage = "Campo(s) vacío(s)";
            } else {
                NurseService.addExpedient($scope.newPat, localStorage.getItem('username'))
                .success(function (getData){
                    //console.log(getData);
                    $scope.cntrlMessage = getData.message;
                    $location.path('/nurse/addExpedient');
                })
                .error(function (data) {
                    //console.log(data);
                    $scope.cntrlMessage = data.err; // El back arroja un error cuando el usuario ya se encuentra registrado.
                });
          }
        };

        $scope.addConsult = function (){
          //console.log($scope.newUser.username);
            if ($scope.newPat.weight=="" || $scope.newPat.weight==null ||
                    $scope.newPat.size=="" || $scope.newPat.size==null ||
                    $scope.newPat.presion=="" || $scope.newPat.presion==null ||
                    $scope.newPat.pulse=="" || $scope.newPat.pulse==null){
                //console.log("Campo vacio");
                $scope.cntrlMessage = "Campo(s) vacío(s)";
            } else {
                NurseService.addConsult($scope.newPat)
                .success(function (getData){
                    //console.log(getData);
                    $scope.cntrlMessage = getData.message;
                    $location.path('/nurse/addConsult');
                    $scope.newPat = null;
                })
                .error(function (data) {
                    //console.log(data);
                    $scope.cntrlMessage = data.err; // El back arroja un error cuando el usuario ya se encuentra registrado.
                });
          }
        };

        $scope.getConsultHistory = function(){ // se crea una funcion en el controlador
            NurseService.getConsultHistory($scope.newPat.searchPat) // Parametro username.
            .success(function(getData){ // Se requiere del servicio "getUSer()" desde "UserService", en caso de que el servicio resulto sin errores:
                //console.log(angular.toJson(getData.data));
                $scope.listInfoConsult = getData.data; // Se obtienen los datos y se mandan a la variable listUsers
                if ($scope.listInfoConsult == 0){ // Si no encontro datos
                    //console.log("undefined 1");
                    $scope.cntrlMessage = "No Hay Historial";
                    // Deshabilita los botones de eliminar y actualizar.
                    $scope.havePatiens=false;
                } else{
                    $scope.havePatiens=true;
                    $scope.cntrlMessage ="";
                    $scope.searchConsultHistory = $scope.listInfoConsult[0]; // Si hay registros, Coloca el primero en el combobox para delUSer y para updUser
                } //console.log($scope.delUser);
            })
            .error(function(data) { // En caso contrario, se imprime el error en consola.
                console.log('Error:');
                console.log(data);
            });

        };

        $scope.getPatientHistory = function(){ // se crea una funcion en el controlador
            NurseService.getPatientHistory($scope.newPat.searchPat) // Parametro username.
            .success(function(getData){ // Se requiere del servicio "getUSer()" desde "UserService", en caso de que el servicio resulto sin errores:
                //console.log(angular.toJson(getData.data));
                $scope.newPat.listInfo = getData.data; // Se obtienen los datos y se mandan a la variable listUsers
                if ($scope.newPat.listInfo == 0){ // Si no encontro datos
                    //console.log("undefined 1");
                    $scope.cntrlMessage = "No hay usuarios";
                    // Deshabilita los botones de eliminar y actualizar.
                    $scope.havePatiens=false;
                } else{
                    $scope.havePatiens=true;
                    $scope.cntrlMessage ="";
                    $scope.searchConsultHistory = $scope.newPat.listInfo[0]; // Si hay registros, Coloca el primero en el combobox para delUSer y para updUser
                } //console.log($scope.delUser);
            })
            .error(function(data) { // En caso contrario, se imprime el error en consola.
                console.log('Error:');
                console.log(data);
            });

        };

        $scope.getPatients = function(){ // se crea una funcion en el controlador
            NurseService.getPatients()
            .success(function(getData){ // Se requiere del servicio "getUSer()" desde "UserService", en caso de que el servicio resulto sin errores:
                //console.log(angular.toJson(getData.data));
                $scope.newPat.listUsers = getData.data; // Se obtienen los datos y se mandan a la variable listUsers
                if ($scope.newPat.listUsers == 0){ // Si no encontro datos
                    //console.log("undefined 1");
                    $scope.cntrlMessage = "No hay usuarios registrados";
                    $scope.isDisabled = true; // Deshabilita los botones de eliminar y actualizar.
                } else{
                    $scope.newPat.searchPat = $scope.newPat.listUsers[0]; // Si hay registros, Coloca el primero en el combobox para delUSer y para updUser
                } //console.log($scope.delUser);
            })
            .error(function(data) { // En caso contrario, se imprime el error en consola.
                console.log('Error:');
                console.log(data);
            });
            $scope.fecha();
        };

        $scope.fecha = function (){
          var today = new Date();
          var dd = today.getDate();
          var mm = today.getMonth()+1; //January is 0!
          var hh = today.getHours();
          var min = today.getMinutes();
          var ss = today.getSeconds();

          var yyyy = today.getFullYear();
          if(dd<10){
              dd='0'+dd
          }
          if(mm<10){
              mm='0'+mm
          }
          var dateToday = yyyy+'-'+mm+'-'+dd+" "+hh+":"+min+":"+ss;
          //console.log(dateToday);
          $scope.newPat.date = dateToday;
          //console.log($scope.newPat.date);
        }
});
