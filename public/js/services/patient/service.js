'use strict';

angular.module('consultingApp')
    .factory('PatientService', function ($http){ // Servicios del usuario
        return {
            checkAssigned: function (data){ // Request al Back para obtener a todos los usuarios de la BD.
                return $http.get('/api/patient/getAssigned/' + data);
            },
            addSolicitud: function (data){ // Request al Back para obtener a todos los usuarios de la BD.
                return $http.get('/api/patient/addSolicitud/' + data);
            }
        }
    });
