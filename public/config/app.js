'use strict';

angular.module('consultingApp', ['ngRoute']) // Configuracion de las URL desde Front
    .config(function ($routeProvider){ // Se requiere del servicio de routeProvider
        $routeProvider
            .when('/', { // Cuando la ruta sea: localhost:3000/
                templateUrl  : '../views/main/welcome.html', // Como es la primer vista y no se requiere de operaciones en ella, no se requiere de un controlador
            })
            .when('/doctor/addExpedient', { // Cuando la ruta sea: localhost:3000/
                templateUrl  : '../views/doctor/addExpedient.html', // Como es la primer vista y no se requiere de operaciones en ella, no se requiere de un controlador
                controller   : 'DocController',
                controllerAs : 'doc',
                resolve: {
                    factory: checkSession
                }
            })
            .when('/doctor/ConsultHistory', { // Cuando la ruta sea: localhost:3000/
                templateUrl  : '../views/doctor/consultHistory.html', // Como es la primer vista y no se requiere de operaciones en ella, no se requiere de un controlador
                controller   : 'DocController',
                controllerAs : 'doc',
                resolve: {
                    factory: checkSession
                }
            })
            .when('/doctor/MedicalHistory', { // Cuando la ruta sea: localhost:3000/
                templateUrl  : '../views/doctor/expedient.html', // Como es la primer vista y no se requiere de operaciones en ella, no se requiere de un controlador
                controller   : 'DocController',
                controllerAs : 'doc',
                resolve: {
                    factory: checkSession
                }
            })
            .when('/doctor/signup', { // Cuando la ruta sea: localhost:3000/
                templateUrl  : '../views/doctor/signup.html', // Como es la primer vista y no se requiere de operaciones en ella, no se requiere de un controlador
                controller   : 'DocController',
                controllerAs : 'doc'
            })
            .when('/doctor/videocall', { // Cuando la ruta sea: localhost:3000/
                templateUrl  : '../views/doctor/videocall.html', // Como es la primer vista y no se requiere de operaciones en ella, no se requiere de un controlador
                controller   : 'DocController',
                controllerAs : 'doc'
            })
            .when('/doctor/welcome', { // Cuando la ruta sea: localhost:3000/
                templateUrl  : '../views/doctor/welcome.html', // Como es la primer vista y no se requiere de operaciones en ella, no se requiere de un controlador
                controller   : 'DocController',
                controllerAs : 'doc'
            })
            .when('/home', { // Cuando la ruta sea: localhost:3000/
                templateUrl  : '../views/main/home.html', // Como es la primer vista y no se requiere de operaciones en ella, no se requiere de un controlador
                resolve: {
                    factory: checkSession
                }
            })
            .when('/nurse/addPatient', { // Cuando la ruta sea: localhost:3000/
                templateUrl  : '../views/nurse/addPatient.html', // Como es la primer vista y no se requiere de operaciones en ella, no se requiere de un controlador
                controller   : 'NurseController',
                controllerAs : 'nurse',
                resolve: {
                    factory: checkSession
                }
            })
            .when('/nurse/addExpedient', { // Cuando la ruta sea: localhost:3000/
                templateUrl  : '../views/nurse/addExpedient.html', // Como es la primer vista y no se requiere de operaciones en ella, no se requiere de un controlador
                controller   : 'NurseController',
                controllerAs : 'nurse',
                resolve: {
                    factory: checkSession
                }
            })
            .when('/nurse/addConsult', { // Cuando la ruta sea: localhost:3000/
                templateUrl  : '../views/nurse/addConsult.html', // Como es la primer vista y no se requiere de operaciones en ella, no se requiere de un controlador
                controller   : 'NurseController',
                controllerAs : 'nurse',
                resolve: {
                    factory: checkSession
                }
            })
            .when('/nurse/receipt', { // Cuando la ruta sea: localhost:3000/
                templateUrl  : '../views/nurse/receipt.html', // Como es la primer vista y no se requiere de operaciones en ella, no se requiere de un controlador
                controller   : 'NurseController',
                controllerAs : 'nurse',
                resolve: {
                    factory: checkSession
                }
            })
            .when('/patient/videocall', { // Cuando la ruta sea: localhost:3000/
                templateUrl  : '../views/patient/videocall.html', // Como es la primer vista y no se requiere de operaciones en ella, no se requiere de un controlador
                controller   : 'PatientController',
                controllerAs : 'patient',
                resolve: {
                    factory: checkSession
                }
            })
            .when('/patient/videocall/start', { // Cuando la ruta sea: localhost:3000/
                templateUrl  : '../views/patient/videocallStart.html', // Como es la primer vista y no se requiere de operaciones en ella, no se requiere de un controlador
                controller   : 'PatientController',
                controllerAs : 'patient',
                resolve: {
                    factory: checkSession
                }
            })
            .when('/personal', { // Cuando la ruta sea: localhost:3000/
                templateUrl  : '../views/personal/addPatient.html', // Como es la primer vista y no se requiere de operaciones en ella, no se requiere de un controlador
                controller   : 'PersonalController',
                controllerAs : 'personal',
                resolve: {
                    factory: checkSession
                }
            })
            .when('/signin', { // Cuando la ruta sea: localhost:3000/users
                templateUrl  : '../views/authentication/signin.html', // Se requiere de la vista y el controlador, asi como un alias del mismo.
                controller   : 'AuthController',
                controllerAs : 'auth',
            })
            .when('/signout', { // Cuando la ruta sea: localhost:3000/signout
                templateUrl  : '../views/authentication/signout.html',
                controller   : 'AuthController',
                controllerAs : 'auth',
                resolve: {
                    factory: checkSession
                }
            })
            .when('/users', { // Cuando la ruta sea: localhost:3000/users
                templateUrl  : '../views/users/userAdministrator.html',// Como es una vista con solo enlaces y no se requiere de operaciones en ella, no se requiere de un controlador
                resolve: {
                    factory: checkSession
                }
            }) // *************MODIFICAR vista Y controlador
            .when('/users/add', { // Cuando la ruta sea: localhost:3000/users/add
                templateUrl  : '../views/users/addUser.html', // Se requiere de la vista y el controlador, asi como un alias del mismo.
                controller   : 'UserController',
                controllerAs : 'user',
                resolve: {
                    factory: checkSession
                }
            })
            .when('/users/delete', { // Cuando la ruta sea: localhost:3000/users/delete
                templateUrl  : '../views/users/deleteUser.html',
                controller   : 'UserController',
                controllerAs : 'user',
                resolve: {
                    factory: checkSession
                }
            }) // *************MODIFICAR vista Y controlador
            .when('/users/update', { // Cuando la ruta sea: localhost:3000/users/add
                templateUrl  : '../views/users/updateUser.html', // Se requiere de la vista y el controlador, asi como un alias del mismo.
                controller   : 'UserController',
                controllerAs : 'user',
                resolve: {
                    factory: checkSession
                }
            }) // *************MODIFICAR vista Y controlador
            .when('/users/search', { // Cuando la ruta sea: localhost:3000/users/delete
                templateUrl  : '../views/users/searchUser.html',
                controller   : 'UserController',
                controllerAs : 'user',
                resolve: {
                    factory: checkSession
                }
            })
            .otherwise({
                redirectTo : '/doctor/welcome'
            });
    });

    var checkSession= function ($q, $rootScope, $location) { // Revisa si hay usuario en linea para accesos indevidos.
        if (!localStorage.getItem('username') || localStorage.getItem('username') == null || !localStorage.getItem('type') || localStorage.getItem('type') == null){
              //location.href = '#/';
              $location.path('/');
        }
    };
