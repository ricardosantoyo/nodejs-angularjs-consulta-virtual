angular.module('consultingApp')
    .controller('PatientController', function($scope, PatientService){ // Controlador de Usuarios manejando y requiriendo los servicios de scope, (MainService) userService.js y http
        $scope.isAssigned = false;
        $scope.isOnline = false;
        $scope.isBusy = true;
        $scope.estilo = {
            color: "#FCDD15"
        };

        $scope.checkAssigned = function(){ // se crea una funcion en el controlador
            PatientService.checkAssigned(localStorage.getItem('username')) // Parametro username.
            .success(function(getData){
                //console.log(angular.toJson(getData.data));
                //console.log(getData.data);
                //console.log(getData.data[0].online);
                $scope.mydata = getData.data; // Se obtienen los datos y se mandan a la variable listUsers
                if ($scope.mydata != 0){ // Si no encontro datos
                    $scope.isAssigned = true;
                    if (getData.data[0].online!=0){
                        $scope.isOnline=true;
                        if (getData.data[0].online==1){
                            $scope.estilo = {
                                color: "#009900"
                            };
                            $scope.isBusy = false;
                            localStorage.setItem('doctor',getData.data[0].username);
                            $scope.initVid();
                        } else{
                          $scope.estilo = {
                              color: "#CC0000"
                          };
                        }
                    } else{
                        $scope.estilo = {
                            color: "#A0A0A0"
                        };
                    }
                }
            })
            .error(function(data) { // En caso contrario, se imprime el error en consola.
                console.log('Error:');
                console.log(data);
            });

        };

        // Cuando se añade un nuevo USER, manda el texto a la API
        $scope.signup = function (){
          //console.log($scope.newUser.username);
            if ($scope.newDoc.username=="" || $scope.newDoc.username==null ||
                    $scope.newDoc.pass1=="" || $scope.newDoc.pass1==null ||
                    $scope.newDoc.pass2=="" || $scope.newDoc.pass2==null ||
                    $scope.newDoc.name=="" || $scope.newDoc.name==null ||
                    $scope.newDoc.lastnameFather=="" || $scope.newDoc.lastnameFather==null ||
                    $scope.newDoc.lastnameMother=="" || $scope.newDoc.lastnameMother==null ||
                    $scope.newDoc.email=="" || $scope.newDoc.email==null ||
                    $scope.newDoc.sex=="" || $scope.newDoc.sex==null){
                //console.log("Campo vacio");
                $scope.cntrlMessage = "Campo(s) vacío(s)";
            } else if ($scope.newDoc.pass1 != $scope.newDoc.pass2){
                $scope.cntrlMessage = "Las contraseñas no concuerdan";
            } else{
                DocService.signup($scope.newDoc)
                .success(function (getData){
                    //console.log(getData);
                    $scope.cntrlMessage = getData.message;
                    $scope.newDoc = null;
                })
                .error(function (data) {
                    //console.log(data);
                    $scope.cntrlMessage = data.err; // El back arroja un error cuando el usuario ya se encuentra registrado.
                });
            }
        };

        $scope.initVid = function(){
          var video_out = document.getElementById("vid-box");
          $scope.phone = window.phone = PHONE({
              number        : localStorage.getItem('username') || "Anonymous", // listen on username line else Anonymous
              publish_key   : 'pub-c-741f5214-a918-4243-803f-24400f84de6b', // Your Pub Key
              subscribe_key : 'sub-c-61a8bc18-39cd-11e6-b47e-0619f8945a4f', // Your Sub Key
              ssl           : (('https:' == document.location.protocol) ? true : false)
          });
          var message = document.getElementById('message');
          $scope.phone.ready(function(){$scope.message = "Conectado satisfactoriamente";});
          $scope.phone.receive(function(session){
            console.log("Recibió llamada");
              session.connected(function(session) {
                video_out.appendChild(session.video);
              });
              session.ended(function(session) {
                video_out.innerHTML='';
              });
          });
        };

        $scope.makeCall = function(){
          console.log("makeCall");
          if (!window.phone) alert("Login First!");
          else {
            console.log("Envió llamada");
            $scope.phone.dial(localStorage.getItem('doctor'));
          }
          return false;
        };
});
